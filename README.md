# Portfolio

## Name
Portfolio of my projects

## Description
This portfolio is a visualization of my skills and projects made while learning Web-developing. My resume is available for downloading. Also it has my contact information and links to GitLab, LinkedIn, email, Whatsapp, etc.
Site works in 3 languages (EN, KZ, RU) using react-i18next.

## Visuals
![Alt text](image-1.png)

![Alt text](image-2.png)

Mobile version
![Alt text](image-4.png)

## Used tools
React (useState, useEffect, Browser Router)
i18next
react-icons
TailwindCSS
HTML

 





